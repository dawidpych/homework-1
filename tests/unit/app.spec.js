describe('App', function () {
    var template = '<div> <span class="js-type">application/json</span><span class="js-response"></span><button type="button" class="js-button-label">Action</button><button type="button" class="js-button" data-menu="js-menu"><span class="caret"></span></button><ul class="menu menu--hidden js-menu"><li><a class="menu__list js-ajax-format" data-format="xml" href="#">xml</a></li><li><a class="menu__list js-ajax-format" data-format="json" href="#">json</a></li></ul>',
        event = {
            preventDefault: function () {}
        };

    beforeEach(function() {
        document.body.insertAdjacentHTML('afterbegin', template);
        XMLHttpRequest = function() {
            this.send = function () {}
        };
        App.init();
    });

    it('should be defined', function() {
        expect(App).toBeDefined();
    });

    it('should set host', function () {
        var url = '/newurl',
            requestUrl = null,
            element = document.getElementsByClassName('js-ajax-format'),
            format = element[0].dataset.format;

        XMLHttpRequest = function() {
            this.open = function (metod, url, any) {
                requestUrl = url;
            };
        };

        App.setHost(url);

        element[0].onclick(event);

        expect(requestUrl).toBe(url + '.' + format);
    });

    it('should set prod setup', function () {
        var element = document.getElementsByClassName('js-ajax-format'),
            format = element[0].dataset.format,
            requestKey = null,
            requestOption = null;

        XMLHttpRequest = function() {
            this.setRequestHeader = function (key, option) {
                requestKey = key;
                requestOption = option;
            };
        };

        App.setProd();

        element[0].onclick(event);

        expect(requestKey).toBe('Accept');
        expect(requestOption).toBe('application/' + format);
    });

    it('should set prod setup with diffred click', function () {
        var element = document.getElementsByClassName('js-ajax-format'),
            format = element[1].dataset.format,
            requestKey = null,
            requestOption = null;

        XMLHttpRequest = function() {
            this.setRequestHeader = function (key, option) {
                requestKey = key;
                requestOption = option;
            };
        };

        App.setProd();

        element[1].onclick(event);

        expect(requestKey).toBe('Accept');
        expect(requestOption).toBe('application/' + format);
    });

    it('should set dev setup', function () {
        var element = document.getElementsByClassName('js-ajax-format'),
            format = element[1].dataset.format,
            requestKey = null,
            requestOption = null,
            requestUrl = null,
            url = '/newurl';

        XMLHttpRequest = function() {
            this.open = function (metod, url, any) {
                requestUrl = url;
            };
            this.setRequestHeader = function (key, option) {
                requestKey = key;
                requestOption = option;
            };
        };

        App.setHost(url)
        App.setProd();
        App.setDev();

        element[1].onclick(event);

        expect(requestKey).toBe(null);
        expect(requestOption).toBe(null);
        expect(requestUrl).toBe(url + '.' + format);
    });
});
