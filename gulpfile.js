"use strict";

var gulp = require("gulp"),
    sass = require("gulp-sass"),
    maps = require("gulp-sourcemaps"),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    useref = require('gulp-useref'),
    sync = require("browser-sync").create(),
    modrewrite = require("connect-modrewrite"),
    TestsServer = require('karma').Server;

gulp.task("browser", function () {
    sync.init({
        server: {
            baseDir: "src",
            middleware: [
                modrewrite(["^([^.]+)$ /index.html [L]"])
            ]
        }
    });
});

gulp.task("test", function(done) {
    new TestsServer({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, done).start();
});

gulp.task("css", function () {
    gulp.src("src/sass/**/style.sass")
        .pipe(maps.init())
        .pipe(sass({ outputStyle: 'compressed' }).on("error", sass.logError))
        .pipe(maps.write("."))
        .pipe(gulp.dest("./src/assets/css/"));
});

gulp.task("watch", ["test", "browser"], function () {
    gulp.watch("src/sass/**/*.sass", ["css"]);
    gulp.watch("tests/**/*.spec.js", ["test"]);
    gulp.watch("src/*.html", sync.reload);
    gulp.watch("src/assets/js/**/*.js", ["test"], sync.reload);
    gulp.watch("src/assets/css/**/*.css", sync.reload);
});

gulp.task('minify', function () {
    gulp.src([
            'src/assets/js/**/app.js',
            'src/assets/js/**/prod.js'
        ])
        .pipe(maps.init())
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest('build/assets/js/'))
});

gulp.task('copy:img',function(){
    gulp.src('src/assets/img/**/*')
        .pipe(gulp.dest('build/assets/img'));
});

gulp.task('copy:css',function(){
    gulp.src('src/assets/css/**/*')
        .pipe(gulp.dest('build/assets/css'));
});

gulp.task('useref', function(){
  return gulp.src('src/index.html')
    .pipe(useref())
    .pipe(gulp.dest('build'))
});

gulp.task('build', ['test', 'minify', 'css', 'copy:css', 'copy:img', 'useref']);
