/**
 * Application object.
 *
 * @param  {window}     w   Window object.
 * @param  {document}   d   Document object.
 * @return {Object}     Return api of application.
 */
var App = (function(w, d) {

    var menuButtonClass = 'js-button',
        labelButtonClass = 'js-button-label',
        spanType = 'js-type',
        spanResponse = 'js-response',
        menuClass = 'js-menu',
        menuHiddenClass = 'menu--hidden',
        ajaxFormatClass = 'js-ajax-format',
        host = '/response',
        prod = false;

    /**
     * Add class to element.
     *
     * @param {HTMLElement}     element     Element when we add class.
     * @param {string}          className   Class name to add.
     */
    function addClass (element, className) {
        if (!element || !className) return;

        if (!new RegExp(className).test(element.className)) {
            element.className += ' ' + className;
        }
    }

    /**
     * Remove class by name.
     *
     * @param  {HTMLElement}    element     Element from we remove class.
     * @param  {string}         className   Class name to remove.
     */
    function removeClass (element, className) {
        if (element && className) {
            var classes = element.className.split(' ');

            if (classes.indexOf(className)!==-1) {
                classes.splice(classes.indexOf(className), 1);
            }

            element.className = classes.join(' ');
        }
    }

    /**
     * Check if element have class name.
     *
     * @param  {HTMLElement}    element     Element when we looking class.
     * @param  {string}         className   Class we looking for.
     * @return {Boolean}                    Return true if find class name.
     */
    function hasClass (element, className) {
        if (!className) {
            return false;
        }

        return new RegExp(className).test(element.className);
    }

    /**
     * Insert text to elements
     *
     * @param  {HTMLCollection}     elements    Elements with we will insert text.
     * @param  {string}             text        String witch will insert.
     */
    function innerText (elements, text) {
        if (typeof text !== 'string') return;

        for (var i=0; i<elements.length; i++) {
            elements[i].innerText = text;
        }
    }

    /**
     * Change button text.
     *
     * @param  {string} text [description]
     */
    function changeButtonText (text) {
        var buttons = d.getElementsByClassName(labelButtonClass);

        innerText(buttons, text);
    }

    /**
     * Event for show menu in button.
     *
     * @param  {Event}  event   Event.
     */
    function onClickMenuButton (event) {
        var element = d.getElementsByClassName(menuClass);

        for (var i=0; i<element.length; i++) {
            if(hasClass(element[i], menuHiddenClass)) {
                removeClass(element[i], menuHiddenClass);
            } else {
                addClass(element[i], menuHiddenClass);
            }
        }
    }

    /**
     * Event for click link in menu.
     *
     * @param  {Event}  event   Event.
     */
    function onLinkClick (event) {
        event.preventDefault();

        if (!this.dataset.format) return;

        var format = this.dataset.format;

        request(host, format);

        onClickMenuButton(event);
    }

    /**
     * Create request and present result.
     *
     * @param  {string}     url     Url to data service.
     * @param  {string}     format  Data format.
     */
    function request (url, format) {
        var header = {};

        changeButtonText(format);

        if (!prod) {
            url = url + '.' + format;
        } else {
            header['Accept'] = 'application/' + format;
        }

        send(url, 'GET', header).then(function (response) {
            innerText(d.getElementsByClassName(spanType), 'application/' + format);
            innerText(d.getElementsByClassName(spanResponse), response);
        });
    }

    /**
     * Create ajax request and return Promise.
     *
     * @param  {string}     url     Url data service.
     * @param  {string}     metod   Method for request ['GET', 'POST', 'HEADER', 'OPTIONS'].
     * @param  {string}     header  Header for request.
     * @return {Promise}            Promise of request.
     */
    function send (url, metod, header) {
        return new Promise(function(resolve, reject) {
            var request = new XMLHttpRequest(),
                metod = metod ? metod : 'GET'

            for (option in header) {
                request.setRequestHeader(option, header[option]);
            }

            request.open(metod, url, true);
            request.onload = function () {
                resolve(request.response);
            };
            request.onerror = function () {
                reject({
                    status: this.status,
                    statusText: request.statusText
                });
            };
            request.send();
        });
    }

    /**
     * Create event click for menu links.
     */
    function eventForLinkTypeClick () {
        var elements = d.getElementsByClassName(ajaxFormatClass);

        for (var i=0; i < elements.length; i++) {
            elements[i].onclick = onLinkClick
        }
    }

    /**
     * Create event to open menu.
     */
    function eventForMenuButtonClick () {
        var elements = d.getElementsByClassName(menuButtonClass);

        for (var i=0; i < elements.length; i++) {
            elements[i].onclick = onClickMenuButton
        }
    }

    /**
     * Set production mode.
     */
    function setProd () {
        prod = true;
    }

    /**
     * Set developent mode.
     */
    function setDev () {
        prod = false;
    }

    /**
     * Set host url.
     * @param {string} newHost New url address.
     */
    function setHost (newHost) {
        host = newHost
    }
    /**
     * Init metod for application.
     */
    function init() {
        eventForMenuButtonClick();
        eventForLinkTypeClick();

        request(host, 'json');

        if (typeof hljs !== 'undefined') {
            hljs.initHighlightingOnLoad();
        }
    }

    return {
        setDev: setDev,
        setProd: setProd,
        setHost: setHost,
        init: init
    };
})(window, document);
