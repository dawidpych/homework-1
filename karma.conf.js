module.exports = function (config) {
  var _config = {
    basePath: '',

    frameworks: ['jasmine'],

    files: [
        { pattern: 'node_modules/promise-polyfill/promise.js', watched: false },
        'src/**/domclass.js',
        'src/**/http.js',
        'src/**/app.js',
        'tests/unit/**/*.spec.js'
    ],

    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false
  };

  config.set(_config);
};
