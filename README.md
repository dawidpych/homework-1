# Application

## Instalation

To create aplication you must firts run

```javascript
npm install
```

## Work with application

You can run application by
```javascript
gulp browser
```

Build application

```javascript
gulp build
```

Develop work.

```javascript
gulp watch
```

Run tests


```javascript
gulp test
```
or


```javascript
npm run test
```
